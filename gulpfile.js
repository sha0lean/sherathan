/* ----------------------------------------------------------------------------------- */
// on declare nos dépendances gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

/* ----------------------------------------------------------------------------------- */
// Sass utilise le compiler node.js
sass.compiler = require('node-sass');

/* ----------------------------------------------------------------------------------- */
// Création d'une tâche pour compiler les fichiers SASS en CSS
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())

    //  .hook(compresse)                         .ou(console log l'erreur) 
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))

        .pipe(autoprefixer())
        .pipe(sourcemaps.write())

        .pipe(gulp.dest('./build/css'))

        .pipe(browserSync.stream());
});

/* ----------------------------------------------------------------------------------- */
// COPY l'HTML dans "BUILD" le fichier client
gulp.task('html', function () {
    return gulp.src('./src/*.html')
        .pipe(gulp.dest('./build'))
});
/* ----------------------------------------------------------------------------------- */
// COPY les ASSETS dans "BUILD" le fichier client
gulp.task('assets', function () {
    return gulp.src('./src/assets/**')
        .pipe(gulp.dest('./build/assets'))
});

/* ----------------------------------------------------------------------------------- */
// WATCHER pour surveiller les changements sur les fichiers
gulp.task('watch', function () {
    browserSync.init({
        server: {
            //cree un server sur build 
            baseDir: "./build"
        }
    });

    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'))
    gulp.watch("./src/**/*.html", gulp.series('html')).on('change', browserSync.reload);

});

/* ----------------------------------------------------------------------------------- */
// type "gulp" to run them all
gulp.task('default', gulp.parallel('sass', 'html', 'watch', 'assets'));


